#!/usr/bin/perl


$file=$ARGV[0];

if ($file=~/transient/)
{

	$basefile="tableall";
}
elsif($file=~/stable/)
{
	$basefile="stableshits";
}

$cutoff=$ARGV[1];
if (!$cutoff)
{
  $cutoff=0.1;
}


`./cleancarryover.pl $file $basefile > $file.nco`;

`./calcfdr.pl $file.nco`;
`./getfdr1prc.pl $file`;

open (IN,"$file.coind")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  
  if ($a[4]=~/DECOY/ || $_=~/^Bait/i || $a[4] eq 'GFP_AEQVI' || $a[4] eq 'TRYP_PIG'|| $a[1]=~/Citrine/i || $a[4] eq 'Cherry' || $a[4]=~/_HUMAN/ )
  {
    next;
  }
  $bait{$a[1]}=$a[3];
  if ($a[3] eq $a[4])
  {
    $baitfactor{$a[1]}=$a[9]/$a[11];
  }
  elsif ($a[11])
  {

    $a=$a[9]/$a[11]; #normalize total reads per length
    $pair=$a[1]."\t".$a[4];
    $score{$pair}=$a;
  }
  $isabait{$a[3]}=1;
  $pair2=$a[3]."\t".$a[4];
  $mascot{$pair2}.=$a[8]." ";
  $unipeps{$pair2}.=$a[10]." ";
  $totunipeps{$pair2}+=$a[10];
  $totpeps{$pair2}+=$a[9];
  $samplespair{$pair2}.=$a[1]." ";
  $samplespaircount{$pair2}++;
  $description{$pair2}=$a[7];
  $samples{$a[1]}=1;
  $baitsamples{$a[3]}.=$a[1].", ";
  $prey{$a[4]}=1;
  if ($preysperbait{$a[3]}!~/$a[4] /)
  {
    $preysperbait{$a[3]}.=$a[4]." ";
  }
}
close IN;

foreach $p(keys %mascot)
{
  @tmp=split(/ /,$mascot{$p});
  if ($#tmp==0)
  {
    $mascot{$p}=$tmp[0];
  }
  elsif($#tmp==1)
  {
    $mascot{$p}=($tmp[0]+$tmp[1])/2;
  }
  else
  {
    @tmp=sort { $b <=> $a } @tmp;
    $mascot{$p}=($tmp[0]+$tmp[1])/2;
  }
  
}

foreach $a (keys %score)
{
  @a=split(/\t/,$a);
  $score{$a}*=$baitfactor{$a[0]}; #normalize per normalized bait peptides
}

foreach $bait(keys %baitsamples)
{
  $countbaits++;
  @a=split(/, /,$baitsamples{$bait});
  @b=split(/ /,$preysperbait{$bait});
  @tmp=();
  foreach $b1(@b)
  {
    foreach $a1(@a)
    {
      $pair=$a1."\t".$b1;
      if ($score{$pair})
      {
	push @tmp,$score{$pair};
      }
     
    }
     @tmp=sort {$b <=> $a} @tmp;
      $pair2=$bait."\t".$b1;
      $scoremerged{$pair2}=($tmp[0]+$tmp[1])/2; #merge the scores from the replicates to avoid bias
      @tmp=();
    $scorespreys{$b1}.=$scoremerged{$pair2}."\t";   
  }
}
#make random arrays of prey scores
$n=2*int($countbaits/3);
foreach $prey (keys %scorespreys)
{
  @a=split(/\t/,$scorespreys{$prey});
  $i=0;
  @done=();
  while ($i<=$n)
  {
    $j=int(rand($countbaits));
    if (!$done[$j])
    {
      if (!$a[$j])
      {
	$a[$j]=0;
      }
      $randpreys{$prey}[$i]=$a[$j];
      $i++;
      $done[$j]=1;
    }
  }
  @{$randpreys{$prey}}=sort {$b<=>$a} @{$randpreys{$prey}};
   
}


foreach $prey (keys %randpreys)
{
    @a=@{$randpreys{$prey}};
	foreach $i(0..$#a)
	{
		$avg+=$a[$i];
	}
	$avg/=($#a+1);
	foreach $i(0..$#a)
	{
		$stdev=($a[$i]-$avg)**2;
	}
	$stdev=sqrt($stdev/$#a);
	$piugold{$prey}=$avg+ (2.58 * $stdev);
#	$pilgold{$prey}=$avg- (2.58 * $stdev);
	$piusilver{$prey}=$avg+ (1.96 * $stdev);
#	$pilsilver{$prey}=$avg- (1.96 * $stdev);
}




open (OUT,">$file.allstatvalues")||die;
print OUT "Bait\tPrey\tStatvalue\tAvgMascot\tAvgNormPeps\tPIugold\tPIusilver\n";
foreach $pair(keys %scoremerged)
{

  @a=split(/\t/,$pair);
  $pair1=$a[1]."\t".$a[0];
  $pvalue=1;
  $n=10000;
  if ($a[0] eq $a[1])
  {
  	next;
  }
  foreach $i(0..$#{$randpreys{$a[1]}})
  {
    $n=$i;
    if ($scoremerged{$pair}>=$randpreys{$a[1]}[$i])
    {
      last;
    }
  }
  $pvalue=$n/($#{$randpreys{$a[1]}}+1);
  
  print OUT $pair,"\t",$pvalue,"\t",$mascot{$pair},"\t",$scoremerged{$pair},"\t",$piugold{$a[1]},"\t",$piusilver{$a[1]},"\n";
  if ($scoremerged{$pair}>$piugold{$a[1]} || ($scoremerged{$pair} && $scoremerged{$pair1}))#$pvalue<$cutoff 
  {
    $keeppair{$pair}=$pvalue;
     $flagscored{$pair}=1;
    $baitskept{$a[0]}=1;
    $preyskept{$a[1]}=1;
  }
}
close OUT;


open (OUT,">$file.out")||die;
print OUT "Bait\tPrey\tScore\tStatvalueAcrossBaits\tMascot\tStatvalueSameBait\tPIugold\tPIusilver\n";
foreach $b(keys %baitskept)
{
  @tmp=();
  @rand=();
  foreach $p(keys %preyskept)
  {
    $pair=$b."\t".$p;
    if ($flagscored{$pair} && $scoremerged{$pair})
    {
      push @tmp,$scoremerged{$pair};
    }
  }
  @tmp=sort {$b <=> $a} @tmp;
  $n=2*(int($#tmp));
  $i=0;
  @keep=();
  while($i<$n)
  {
    $j=int(rand($#tmp));
    if (!$kept[$j])
    {
      push @rand,$tmp[$j];
      $keep[$j]=1;
      $i++;
    }
  }

  @rand=sort {$b<=>$a} @rand;

  foreach $p(keys %preyskept)
  {
    $pair=$b."\t".$p;
    if ($scoremerged{$pair} && $flagscored{$pair})
    {
      $rank=$#rand;
      foreach $i(0..$#rand)
      {
			if ($scoremerged{$pair}>$rand[$i])
			{
			  $rank=$i;
			  last;
			}
     }
      $pvalue=$rank/$#rand;
      $pvalues{$pair}=$pvalue;
      print OUT $pair,"\t",$scoremerged{$pair},"\t",$keeppair{$pair},"\t",$mascot{$pair},"\t",$pvalue,"\t",$piugold{$p},"\t",$piusilver{$p},"\n";
    }
  }
}

close OUT;

%baits=();
%preys=();
%line=();
%preyind=();
$countbaits=0;
%frequency=();

open(IN,"$file.out")||die;
while(<IN>)
{
  chomp;
  if ($_=~/^Bait/)
  {
    next;
  }
  @a=split/\t/;
  $baits{$a[0]}=1;
  $preys{$a[1]}++;
  $pair=$a[0]."\t".$a[1];
  
  $line{$pair}=$_;
  $preyind{$pair}=$a[1];
}
close IN;

foreach $b(keys %baits)
{
  $countbaits++;
}

foreach $a(keys %preys)
{
  $freq=$preys{$a}/$countbaits;
    $frequency{$a}=$freq;
}

open (OUT,">$file.outfreq" )||die;
print  OUT "Bait\tPrey\tScore\tStatvalueAcrossBaits\tMascot\tStatvalueSameBait\tPIugold\tPIusilver\tFrequency\n";
foreach $p(keys %line)
{
  print OUT $line{$p},"\t",$frequency{$preyind{$p}},"\n";
}
close OUT;

`./roccurvefreq.pl $file $basefile > $file.freqmetrics`;

$max=0;
$bestfreq=0;
$prec75freq=0;
open (IN,"$file.freqmetrics")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  if ($a[3]>$max)
  {
    $max=$a[3];
    $bestfreq=$a[0];
  }
  if ($a[1]>0.75 && $prec80freq==0)
  {
    $prec75freq=$a[0];
  }
}
close IN;

`./roccurvescores.pl $file $basefile > $file.scoresmetrics`;

$max=0;
$bestscore=0;
$prec75score=0;
open (IN,"$file.scoresmetrics")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  if ($a[3]>$max)
  {
    $max=$a[3];
    $bestscore=$a[0];
  }
  if ($a[1]>0.75 && $prec75score==0)
  {
    $prec75score=$a[0];
  }
}
close IN;

open (IN,"crapome")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  $crap{$a[1]}=$a[0];
}
close IN;
%keep=();
open (OUT,">$file.allinfo")||die;
open (IN,"$file.outfreq")||die;
while(<IN>)
{
  chomp;	
  if ($_=~/^Bait/)
  {
   $header=$_."\tUniquePeps\tTotalUP\tCombiUP\tCombiSamples\tSamples\tSamplesCount\tDescription\n";
    print OUT $_,"\tUniquePeps\tTotalUP\tCombiUP\tCombiSamples\tSamples\tSamplesCount\tDescription\n";
  }
  else
  {
    @a=split/\t/;
    $pair=$a[0]."\t".$a[1];
    $baitflag=0;
    if ($isabait{$a[0]}==1 && $isabait{$a[1]}==1 )
    {
      $baitflag=1;
    }
    $pair1=$a[1]."\t".$a[0];
    $combipairs=$totunipeps{$pair}+$totunipeps{$pair1};
    $combisample=$samplespaircount{$pair}+$samplespaircount{$pair1};
   # $combipairstot=($totpeps{$pair}+$totpeps{$pair1})/$combisample;
    
    
    #if ($cherrypeps{$a[1]})
    #{
   # 	$chc=$cherrypeps{$a[1]}/$cherrynumb{$a[1]};
  #  	$ratio= $combipairstot/$chc; #add here
    	#print $a[0],"\t",$a[1],"\t",$combipairstot,"\t",$combicherry,"\t",$cherrypeps{$a[0]},"\t",$cherrypeps{$a[1]},"takis\n";
    #}
    #else
    #{
   # 	$ratio=5;
    #}
    
    
  print OUT $_,"\t",$unipeps{$pair},"\t",$totunipeps{$pair},"\t",$combipairs,"\t",$combisample,"\t",$samplespair{$pair},"\t",$samplespaircount{$pair},"\t",$description{$pair},"\n";
  if ($file=~/transient/)
  {
     if ( $crap{$a[1]}>2 || $a[6]>0.1 || ($a[2]<$bestscore && $a[6]>$bestfreq)|| ($crap{$a[1]}>0 && $a[2]<$bestscore) || ( $combisample==1 && $combipairs<3 && $baitflag==0) || ( $combisample==1 && $combipairs<2 && $baitflag==1) || $combipairs==1 ||($combisample==1 && $a[6]>$prec75freq)) #for transient
#      if ($crap{$a[1]}>2 || $a[6]>0.1 || ($a[2]<$bestscore && $a[6]>$bestfreq)|| ($crap{$a[1]}>0 && $a[2]<$bestscore) || ( $combisample==1 && $combipairs<5 && $baitflag==0) || ( $combisample==1 && $combipairs<2 && $baitflag==1) || $combipairs<5 || $combisample==1 ||($combisample==1 && $a[6]>$prec75freq)) #forstable
    {
      next;
    }
    else
    {
      $keep{$pair}=$_."\t".$unipeps{$pair}."\t".$totunipeps{$pair}."\t".$combipairs."\t".$combisample."\t".$samplespair{$pair}."\t".$samplespaircount{$pair}."\t".$description{$pair};
    }
   }
   if ($file=~/stable/)
   {
#   	 if ( $crap{$a[1]}>2 || $a[6]>0.1 || ($a[2]<$bestscore && $a[6]>$bestfreq)|| ($crap{$a[1]}>0 && $a[2]<$bestscore) || ( $combisample==1 && $combipairs<3 && $baitflag==0) || ( $combisample==1 && $combipairs<2 && $baitflag==1) || $combipairs==1 ||($combisample==1 && $a[6]>$prec75freq)) #for transient
      if ($crap{$a[1]}>2 || $a[6]>0.1 || ($a[2]<$bestscore && $a[6]>$bestfreq)|| ($crap{$a[1]}>0 && $a[2]<$bestscore) || ( $combisample==1 && $combipairs<5 && $baitflag==0) || ( $combisample==1 && $combipairs<2 && $baitflag==1) || $combipairs<5 || $combisample==1 ||($combisample==1 && $a[6]>$prec75freq)) #forstable
    {
      next;
    }
    else
    {
      $keep{$pair}=$_."\t".$unipeps{$pair}."\t".$totunipeps{$pair}."\t".$combipairs."\t".$combisample."\t".$samplespair{$pair}."\t".$samplespaircount{$pair}."\t".$description{$pair};
    }
   }
   
   
   
  }
}
close IN;
close OUT;



`./runsscoreontop.pl $file > $file.clean`;
`./implementcompass.pl $file.clean > $file.clean.compass`;
`./roccurvecompass.pl $file $basefile > $file.clean.sscoremetrics`;

$max=0;
$bestsscore=0;
$recall7sscore=0;

open (IN,"$file.clean.sscoremetrics")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  if ($a[3]>$max)
  {
    $max=$a[3];
    $bestsscore=$a[0];
  }
  if ($a[2]>0.7)
  {
    $recall7sscore=$a[0];
  }
}
close IN;
#print $bestsscore,"\t",$recall7sscore,"\n";

open (IN,"$file.clean.compass")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  $pair=$a[0]."\t".$a[1];
    $compass{$pair}=$a[2];
}
close IN;

open (OUT,">$file.goldenres")||die;
open (OUTA,">$file.silverres")||die;
open (OUTB,">$file.copperres")||die;
print OUTB "S-score\t$header\n";
print OUTA "S-score\t$header\n";
print OUT "S-score\t$header\n";
foreach $p (sort keys %keep)
{
  print OUTB $compass{$p},"\t",$keep{$p},"\n";
  if ($recall7sscore>$bestsscore)
  {
  	if ($compass{$p}>=$recall7sscore)
  	{
  	  print OUT $compass{$p},"\t",$keep{$p},"\n";
 	 }
 	 if ($compass{$p}>=$bestsscore)
  	{
  	  print OUTA $compass{$p},"\t",$keep{$p},"\n";
 	 }
  }
  else
  {
  	if ($compass{$p}>=$recall7sscore)
  	{
  	  print OUTA $compass{$p},"\t",$keep{$p},"\n";
  	}
  	if ($compass{$p}>=$bestsscore)
  	{
  	  print OUT $compass{$p},"\t",$keep{$p},"\n";
  	}
  }
  


}
close OUT;
close OUTA;
close OUTB;
