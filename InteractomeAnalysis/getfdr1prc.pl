#!/usr/bin/perl

if ($ARGV[0]=~/ltq1/)
{
    $co=40;
}
else
{
  $co=25;
}

open (IN,"$ARGV[0].nco.fdr")||die ;#$ARGV[0]nco.fdr
while(<IN>)
{
  chomp;
  if ($_=~/^sample/)
  {
    @cutoffs=split/\t/;
  }
  else
  {
    @a=split/\t/;
    $s=$a[0];
    foreach $i(1..$#a)
    {
      if ($a[$i]<=0.01)
      {
	$cutoff{$s}=$cutoffs[$i];
	$fdr{$s}=$a[$i];
	#print $s,"\t",$cutoff{$s},"\n";
	last;
      }
    }
    if ($cutoff{$s} eq '')
    {
      $cutoff{$s}=$cutoffs[$#a];
	$fdr{$s}=$a[$#a];
    }
  }
}
close IN;
#print '2115_239_FAM13A_V4560_M2',"\t",$cutoff{'2115_239_FAM13A_V4560_M2'},"\n";
# exit;
open (IN,"$ARGV[0].nco")||die;
open (OUTA,">$ARGV[0]$co")||die;
open (OUTB,">$ARGV[0].coind")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  if ($a[8]>=$co)
  {
    print OUTA $_,"\n";
  }

#print $a[1],"\t",$cutoff{$a[1]},"\n";
  if ($a[8]>=$cutoff{$a[1]})
  {
    print OUTB $_,"\t",$cutoff{$a[1]},": $fdr{$a[1]}\n";
  }
}
close IN;
close OUTA;
close OUTB;