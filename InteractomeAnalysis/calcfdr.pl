#!/usr/bin/perl

open (IN,"$ARGV[0]")||die;
while(<IN>)
{
  chomp;
  if ($_=~/Sample Name/)
  {
    next;
  }
  @a=split/\t/;
  $sample=$a[1];
  $prot=$a[5];
  $mascot=$a[8];
  $data{$sample}.=$prot."\t".$mascot."\n";
}
close IN;

%decoysall=0;
%otherall=0;

foreach $s (keys %data)
{
  @hits=split(/\n/,$data{$s});
  for($co=25;$co<=50;$co+=5)
  {
    $dec=0;
    $oth=0;
    foreach $a (@hits)
    {
      @tmp=split(/\t/,$a);
      if ($tmp[1]>=$co)
      {
	if ($tmp[0]=~/DECOY/)
	{
	      $dec++;
	      $decoysall{$co}++;
	}
	else
	{
	      $oth++;
	      $otherall{$co}++;
	}
      }
    }
    $fdr=0;
    if ($dec+$oth==0)
    {
      $fdr='NA';
    }
    else
    {
      $fdr=$dec/($dec+$oth);
    }
    $det{$s}[$co]=$fdr;
  }
}

open (OUT,">$ARGV[0].fdr")||die;
print OUT "sample\t25\t30\t35\t40\t45\t50\n";
foreach $s(keys %det)
{
  print OUT $s,"\t";
  for($co=25;$co<=50;$co+=5)
  {
    print OUT $det{$s}[$co],"\t";
  }
  print OUT "\n";
}
close OUT;
