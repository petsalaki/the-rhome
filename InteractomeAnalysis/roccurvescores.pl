#!/usr/bin/perl

$file=$ARGV[0];

open (IN,"knownoverlaphippieall")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
   if ($a[0]>0.75)
  {
    $int=$a[2]."\t".$a[4];
    $known{$int}=1;
    $int=$a[4]."\t".$a[2];
    $known{$int}=1;
  }
}
close IN;
open (IN,"$file.coind")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  $geneid{$a[3]}=$a[0];
  $geneid{$a[4]}=$a[2];
}
close IN;


open (IN,"$ARGV[1].cherry")||die;
while(<IN>)
{
  chomp;
    $cherry{$_}=1;
}
close IN;

$maxscore=0;
$minscore=10000;

open (IN,"$file.outfreq")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  if ($a[0] eq 'Bait')
  {
    next;
  }
  $pair=$a[0]."\t".$a[1];
  $int=$geneid{$a[0]}."\t".$geneid{$a[1]};
  
  if ($a[1]=~/Cherry/i || $_=~/DECOY/)
  {
    next;
  }
  if ($a[2])
  {
    $a1=log($a[2])/log(10);
  }
  else
  {
    $a1=0;
  }

  if ($a1>$maxscore)
  {
    $maxscore=$a1;
  }
  if ($a1<$minscore)
  {
    $minscore=$a1;
  }
  if ($cherry{$a[1]}==1)
  {
    push @negall,log($a[2])/log(10);
  }
  else
  {
    push @allall,log($a[2])/log(10);
  }

  if($known{$int}==1)
  {
    push @knownall,log($a[2])/log(10);
  }
  
}
close IN;

$step=($maxscore-$minscore)/20;


    foreach $a (@negall)
    {
      for ($c=$minscore;$c<=$maxscore;$c+=$step)
      {
	if ($a>=$c)
	{
	    $fpall{$c}++;
	}
	elsif ($a<$c )
	{
	    $tnall{$c}++;
	}
      }
    }
    
    foreach $a (@knownall)
    {
      for  ($c=$minscore;$c<=$maxscore;$c+=$step)
      {
	if ($a>=$c)
	{
	    $tpall{$c}++;
	}
	elsif ($a<$c )
	{
	    $fnall{$c}++;
	}
      }
    }




    foreach $a (@allall)
    {
      for  ($c=$minscore;$c<=$maxscore;$c+=$step)
      {
	if ($a>=$c)
	{
	    $tp1all{$c}++;
	}
	elsif ($a<$c)
	{
	    $fn1all{$c}++;
	}
      }
    }



for($c=$minscore;$c<=$maxscore;$c+=$step)
{
$tp1all{$c}/=($#allall+1);
  $fn1all{$c}/=($#allall+1);
   if ($#knownall==-1)
  {
     $tpall{$c}=0;
    $fnall{$c}=0;
  }
  else
  {
    $tpall{$c}/=($#knownall+1);
    $fnall{$c}/=($#knownall+1);
  }
  if ($#negall+1)
  {
    $tnall{$c}/=($#negall+1);
    $fpall{$c}/=($#negall+1);
  }
  else
  {
    $tnall{$c}=0;
    $fpall{$c}=0;
  }
if ($fpall{$c}==0 && $tpall{$c}==0)
{
  $precision=0;
}
else
{
  $precision=$tpall{$c}/($tpall{$c}+$fpall{$c});
}

if ($tpall{$c}==0 && $fnall{$c}==0)
{
  $precision=0;
}
else
{
  $recall=$tpall{$c}/($tpall{$c}+$fnall{$c});
}

$abc=(sqrt(($tpall{$c}+$fpall{$c})*($tpall{$c}+$fnall{$c})*($tnall{$c}+$fpall{$c})*($tnall{$c}+$fnall{$c})));
if ($abc)
{
  $mcc=($tpall{$c}*$tnall{$c}-$fpall{$c}*$fnall{$c})/(sqrt(($tpall{$c}+$fpall{$c})*($tpall{$c}+$fnall{$c})*($tnall{$c}+$fpall{$c})*($tnall{$c}+$fnall{$c})));
}
else
{
  $mcc=0;
}
print $c,"\t",$precision,"\t",$recall,"\t",$mcc,"\n";



}


