#!/usr/bin/perl

open (IN,"$ARGV[0].allinfo")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  $pair=$a[0]."\t".$a[1];
  $keep{$pair}=1;
}
close IN;

open (IN,"$ARGV[0].coind")||die;
while(<IN>)
{
  chomp;
  @a=split/\t/;
  $pair=$a[3]."\t".$a[4];
  if ($keep{$pair})
  {
    print $_,"\n";
  }
}
close IN;