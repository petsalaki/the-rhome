#!/usr/bin/perl


    $sample=1;
    $hit1=4;
    $hitd=7;
    $bait=3;
$totpep=9;



open (IN,"$ARGV[0]")|| die;
while(<IN>)
{
  chomp;
  @a=split/\t/;

if ($_=~/^Bait/)
{
  next;
}
    $id=$a[$bait];
    $p=$a[$hit1];
  $a1="$id - $p";
  if ($baits!~/^$id$/m)
  {
    $baits.=$id."\n";
  }
  if ($preys!~/^$p$/m)
  {
    $preys.=$p."\n";
  }

  $totpeps{$a1}+=$a[$totpep];
  $count{$a1}++;

}
close IN;


@b=split(/\n/,$baits);
@p=split(/\n/,$preys);

$compasscount=$#b+1;

foreach $a (keys %totpeps)
{
  @tmp=split(/ - /,$a);
  $totpeps_2{$a}/=$count{$a};
  $totpeps{$a}/=$compasscount;
  $avgtotpeps+=$totpeps{$a};
  $avgtotpepscompass{$tmp[1]}+=$totpeps{$a};
  $scorestotpepscompass{$tmp[1]}.=$totpeps{$a}." ";
  $count++;
  push @alltotpeps,$totpeps{$a}; 
}


$avgtotpeps/=$count;


foreach $a (keys %avgtotpepscompass) #get stddev for all preys
{
    @tmp=split(/ /,$scorestotpepscompass{$a});
    $c=$#tmp+1;
    $avgtotpepscompass{$a}/=$compasscount; 
    foreach $a1 (@tmp)
    {
      $stotpepscompass{$a}=$stotpepscompass{$a}+(($a1-$avgtotpepscompass{$a})**2);
    }
    $stotpepscompass{$a}=sqrt($stotpepscompass{$a}/$compasscount); 

  if ($avgtotpepscompass{$a}==0)
  {
     $omegacompass{$a}=0;
  }
  else
  {
    $omegacompass{$a}=$stotpepscompass{$a}/$avgtotpepscompass{$a};
  }
    
}
print "Bait\tPrey\tZ-score\tS-Score\tD-score\tWDscore\n";
foreach $a (keys %totpeps)
{
  @tmp=split(/ - /,$a);
  if ($stotpepscompass{$tmp[1]}!=0)
  {
  $zscorecompass{$a}=($totpeps{$a}-$avgtotpepscompass{$tmp[1]})/$stotpepscompass{$tmp[1]}; 
  }
  else
  {
    $zscorecompass{$a}=($totpeps{$a}-$avgtotpepscompass{$tmp[1]});
  }
@tmp1=split(/ /,$scorestotpepscompass{$tmp[1]});
$n=$#tmp1+1;
  $sscorecompass{$a}=sqrt(($compasscount/$n)*$totpeps{$a});

  $dscorecompass{$a}=sqrt((($compasscount/$n)**$count{$a})*$totpeps{$a});

$wdscorecompass{$a}=sqrt((($compasscount*$omegacompass{$tmp[1]}/$n)**$count{$a})*$totpeps{$a});
print $tmp[0],"\t",$tmp[1],"\t",$zscorecompass{$a},"\t",$sscorecompass{$a},"\t",$dscorecompass{$a},"\t",$wdscorecompass{$a},"\n";
}

